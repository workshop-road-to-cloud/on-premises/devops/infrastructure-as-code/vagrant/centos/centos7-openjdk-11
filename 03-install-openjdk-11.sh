#!/bin/bash
echo "Start script execution---->>03-install-openjdk-11.sh<----"

echo "Step 1: Install Java OpenJDK 11"
sudo yum install -y java-11-openjdk-devel
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
export JRE_HOME=/usr/lib/jvm/jre-11-openjdk
export PATH=$PATH:$JAVA_HOME:$JRE_HOME/bin

echo $PATH
echo $JAVA_HOME
echo $JRE_HOME

echo "End script execution---->>03-install-openjdk-11.sh<<----"